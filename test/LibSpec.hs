module LibSpec (spec) where

import Test.Hspec
import Text.ParserCombinators.Parsec
import Data.Either


import Lib


shouldBeLeft f = isLeft f `shouldBe` True

spec :: Spec
spec = do
    describe "parseCode" $ do
        it "should parse uppercase letters mixed with digits" $
            parse parseCode "" "N1" `shouldBe` Right "N1"

        it "should fail with lowercase" $
            shouldBeLeft $ parse parseCode "" "n1"

    describe "parseDouble" $ do
        it "should parse float" $
            parse parseDouble "" "0.123" `shouldBe` Right 0.123

        it "should fail with integer" $
            shouldBeLeft $ parse parseDouble "" "123"

        it "should fail with characters" $
            shouldBeLeft $ parse parseDouble "" "abc"

    describe "parseAtom" $ do
        it "should parse ATOM structure" $
            let actual = parse parseAtom "" "ATOM A B1 0.12 1.7 3.33333"
                expected = Right (Atom 'A' "B1" 0.12 1.7 3.33333)
            in actual `shouldBe` expected

        it "should fail without prefix" $
            shouldBeLeft $ parse parseAtom "" "A B1 0.12 1.7 3.33333"

        it "should fail without all floats" $
            shouldBeLeft $ parse parseAtom "" "ATOM A B1 0.12 1.7"

        it "should fail without Char" $
            shouldBeLeft $ parse parseAtom "" "ATOM B1 0.12 1.7 3.33"

        it "should fail without code" $
            shouldBeLeft $ parse parseAtom "" "ATOM A 0.12 1.7 1.1"

    describe "parseTer" $ do
        it "should parse TER structure" $
            let actual = parse parseTer "" "TER AB C"
                expected = Right (Ter "AB" 'C')
            in actual `shouldBe` expected

        it "should fail otherwise" $ do
            shouldBeLeft $ parse parseTer "" "AB C"
            shouldBeLeft $ parse parseTer "" "TER AB"
            shouldBeLeft $ parse parseTer "" "TER C"

