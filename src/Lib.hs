module Lib
    -- (
    -- readStream,
    -- parseDouble
    -- )
    where

import Text.ParserCombinators.Parsec

-- ATOM C CA 0.111 0.456 1.345
-- ATOM N N1 0.356 0.789 10.458
-- TER OXT O
-- "ATOM C CA 0.111 0.344 0.222 \nATOM N N1 1.333 2.2 3.1 \nTER OXT O"

data ParsedValue = Atom Char String Double Double Double
                 | Ter String Char
                 deriving (Show, Eq)


readStream :: String -> [String]
readStream input = map
    (processResult . parse readExpr "")
    (filter (not . null) (lines input))


processResult res = case res of
    Left err  -> ""
    Right val -> show val


readExpr :: Parser ParsedValue
readExpr = parseAtom <|> parseTer


parseCode :: Parser String
parseCode = many1 $ upper <|> digit


parseDouble :: Parser Double
parseDouble = do
    int <- many1 digit
    char '.'
    fract <- many1 digit

    return (read (int ++ "." ++ fract) :: Double)


parseAtom :: Parser ParsedValue
parseAtom = do
    string "ATOM"
    many1 space
    symb <- upper
    many1 space
    _code <- parseCode
    many1 space
    x <- parseDouble
    many1 space
    y <- parseDouble
    many1 space
    z <- parseDouble

    return $ Atom symb _code x y z


parseTer :: Parser ParsedValue
parseTer = do
    string "TER"
    many1 space
    code <- parseCode
    many1 space
    symb <- upper

    return $ Ter code symb

