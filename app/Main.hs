module Main where

import Lib
import System.Environment
import System.IO

main :: IO ()
main = do
    (filename:_) <- getArgs

    handle <- openFile filename ReadMode
    input <- hGetContents handle
    print $ readStream input

    hClose handle

